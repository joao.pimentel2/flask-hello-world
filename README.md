# flask-hello-world

A minimal [Flask](https://flask.palletsprojects.com/en/2.0.x/) app.

In order to run a Python web project with Heroku's builder we need:
- a requirements.txt file, a Pipfile, or a setup.py
[Heroku's Python Support](https://devcenter.heroku.com/articles/python-support)
- a [Procfile]
  - [Getting Started on Heroku with Python - Define a Procfile](https://devcenter.heroku.com/articles/getting-started-with-python#define-a-procfile)
  - [The Procfile - Heroku Dev Center](https://devcenter.heroku.com/articles/procfile)

Optionally you can select the Python runtime with a runtime.txt file.
Example: python-3.10.2
