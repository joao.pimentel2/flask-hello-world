from flask import Flask
import os

print('App start up')
print(os.environ.get('PORT', 5005))
app = Flask(__name__)

@app.route("/")
def hello_world():
    framework = os.environ.get('FRAMEWORK', 'this shouldn\'t be visible')
    return '<p>Hello, World! I am built with {}/Python</p>'.format(framework)

if __name__ == '__main__':
    print('starting up')
    port = int(os.environ.get('PORT', 5005))
    print('using port', port)
    app.run(debug=True, host='0.0.0.0', port=port)
